# Rock Paper Scissors

This is a simple rock paper scissors game developed as part of The Odin Project curriculum.
The instructions for the project can be found [here](https://www.theodinproject.com/lessons/foundations-rock-paper-scissors).

## License
This software is provided under the MIT License.

Copyright 2023 David Palubin
