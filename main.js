function getComputerChoice(){
  let selection = Math.floor(Math.random()*3);
  switch(selection){
    case 0:
      return "rock";
      break;
    case 1:
      return "paper";
      break;
    case 2:
      return "scissors";
      break;
    default:
      return "";
      break;
  }
}

function playRound(playerSelection, computerSelection){
  playerSelection = playerSelection.trim().toLowerCase();
  if(!playerSelection || !computerSelection){
    return "There was an error. Sorry!";
  }
  if(playerSelection === computerSelection){
    return "Tie Game! You both chose " + playerSelection + '.';
  }
  switch(playerSelection){
    case "rock":
      if(computerSelection === "paper"){
        return "You lose! The computer chose paper.";
      } else if(computerSelection === "scissors"){
        return "You win! The computer chose scissors.";
      }
      break;
    case "paper":
      if(computerSelection === "rock"){
        return "You win! The computer chose rock."
      } else if(computerSelection === "scissors"){
        return "You lose! The computer chose scissors.";
      }
      break;
    case "scissors":
      if(computerSelection === "rock"){
        return "You lose! The computer chose rock.";
      } else if(computerSelection === "paper"){
        return "You win! The computer chose paper.";
      }
      break;
    default:
      return "Invalid Player Selection!";
      break;
  }
  return "If you are seeing this, I messed up somehow."
}

function game(){
  let playerScore = 0;
  let computerScore = 0;

  for(let gamesPlayed = 1;gamesPlayed <= 5;gamesPlayed++){
    let playerSelection = prompt("Enter 'rock', 'paper', or 'scissors'");
    let computerSelection = getComputerChoice();
    let result = playRound(playerSelection, computerSelection);
    console.log(`Round ${gamesPlayed}: ${result}`);
    if(result.slice(4,7) === "win") playerScore++;
    else if(result.slice(4,8) === "lose") computerScore++;
  }

  if(playerScore > computerScore){
    console.log("You win!");
  } else if (playerScore === computerScore){
    console.log("It's a tie!");
  } else {
    console.log("You lost!");
  }
}

const buttons = Array.from(document.querySelectorAll('button'));
const resultsDiv = document.querySelector('.results');
const playerScoreBoard = document.querySelector('.playerScore');
const computerScoreBoard = document.querySelector('.computerScore');
const finalResult = document.createElement('p');
finalResult.classList.add('finalResult');
finalResult.textContent = '';
const body = document.querySelector('body');

let wins = 0;
let losses = 0;


buttons.forEach(button => button.addEventListener('click',function (e) {
  let playerSelection = e.target.textContent.toLowerCase();
  let computerSelection = getComputerChoice();

  if(wins >= 5 || losses >= 5){
    wins = 0;
    losses = 0;
    playerScoreBoard.textContent = "0";
    computerScoreBoard.textContent = "0";
    body.removeChild(finalResult);
  }

  let result = playRound(playerSelection, computerSelection);
  resultsDiv.textContent = result;
  if(result.slice(4,7) === "win"){
    wins++;
    playerScoreBoard.textContent = wins;
  } 
  else if(result.slice(4,8) === "lose"){
    losses++;
    computerScoreBoard.textContent = losses;
  } 


  if(wins >= 5){
    finalResult.textContent = "You win! First to 5 victories. Press a button again to start over.";
    body.appendChild(finalResult);
  } else if(losses >= 5){
    finalResult.textContent = "You lose! The computer got 5 victories first. Press a button again to start over.";
    body.appendChild(finalResult);
  }
}));